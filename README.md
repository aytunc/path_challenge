###### Mysql

user:root

password:104800

###### Kurulum

`composer install`

`php bin/console doctrine:database:create`

`php bin/console doctrine:schema:create`

`php bin/console doctrine:fixtures:load`

###### Server Run

`php bin/console server:run`

Unit Test

`php ./vendor/bin/phpunit`

###### Swagger

http://localhost:8000/swagger/doc

###### Postman Collection

postman klasörü içerisinde Path - Challenge
