<?php


namespace App\Tests\Controller\Api\Authentication;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class TokenControllerTest extends WebTestCase
{
    public function testPOSTCreateToken()
    {
        $client = static::createClient();
        $client->request('POST', '/api/login', [], [], [
            'HTTP_AUTHORIZATION' => 'Basic YXl0dW5jc2V2cmVuQGdtYWlsLmNvbToxMjM0NTY=',
        ]);
        self::assertResponseStatusCodeSame(200, $client->getResponse()->getStatusCode());
        $this->assertStringContainsString('token', $client->getResponse()->getContent());
    }
}
