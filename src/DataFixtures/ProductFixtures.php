<?php

namespace App\DataFixtures;

use App\Entity\Product;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class ProductFixtures extends Fixture
{
    public function __construct()
    {
    }

    public function load(ObjectManager $manager)
    {
        $product = new Product();
        $product->setName('RAM');
        $manager->persist($product);
        $manager->flush();

        $product = new Product();
        $product->setName('CPU');
        $manager->persist($product);
        $manager->flush();

    }
}
