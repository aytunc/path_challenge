<?php


namespace App\Controller;


use App\Entity\User;
use JMS\Serializer\Context;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\SerializerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class BaseController extends AbstractController
{
    private $tokenStorage;
    private $serializer;
    public function __construct(TokenStorageInterface $storage, SerializerInterface $serializer)
    {
        $this->tokenStorage = $storage;
        $this->serializer = $serializer;
    }

    protected function response($responseData,$serializeGroups = ['Default'])
    {
        if (!is_string($responseData)){
            $context = new SerializationContext();
            $context->setSerializeNull(true);
            $context->setGroups($serializeGroups);
            $responseData = $this->serializer->serialize($responseData, 'json', $context);
        }

        return new Response($responseData, Response::HTTP_OK, [
            'Content-Type' => 'application/json'
        ]);
    }

    protected function deserialize($data,$type)
    {
        return $this->getSerializer()->deserialize($data, $type, 'json');
    }

    /**
     * @return TokenStorageInterface
     */
    public function getTokenStorage(): TokenStorageInterface
    {
        return $this->tokenStorage;
    }

    /**
     * @return SerializerInterface
     */
    public function getSerializer(): SerializerInterface
    {
        return $this->serializer;
    }

    protected function getUser(): User
    {
        return $this->getTokenStorage()->getToken()->getUser();
    }
}