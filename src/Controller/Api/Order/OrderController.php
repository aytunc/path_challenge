<?php


namespace App\Controller\Api\Order;

use App\Controller\BaseController;
use App\Entity\Order;
use DateTime;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use Ramsey\Uuid\Uuid;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;


class OrderController extends BaseController
{
    /**
     * @Route("/orders", methods={"GET"})
     */
    public function orders()
    {
        $orders = $this->getDoctrine()
            ->getRepository(Order::class)
            ->findBy(['user' => $this->getTokenStorage()->getToken()->getUser()]);
        return $this->response($orders);
    }

    /**
     * @Route("/order", methods={"POST"})
     */
    public function orderCreate(Request $request)
    {
        /** @var Order $order */
        $order = $this->deserialize($request->getContent(), Order::class);

        if ($order->getId() > 0) {
            return new Response("id girilemez", "400");
        }
        $uuid = Uuid::uuid4();
        $order->setCode($uuid->toString());
        $order->setShippingDate(new DateTime('+3 days'));
        $order->setUser($this->getUser());
        $order->getItems()->forAll(function ($index,$item) use ($order){
            $item->setOrder($order);
        });

        $em = $this->getDoctrine()->getManager();
        $em->persist($order);
        $em->flush();
        $em->refresh($order);
        return $this->response($order, ['Default','items', 'user']);
    }

    /**
     * @Route("/order/{id}", methods={"PUT"})
     */
    public function orderUpdate(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $order = $em->getRepository(Order::class)->find($id);
        if (!$order) {
            return new Response("sipariş bulunamadı", "404");
        }

        if ($order->getUser()->getId() != $this->getUser()->getId()) {
            return new Response("size ait olmayan siparişi güncelleyemezsiniz", "400");
        }

        $curdate = new DateTime();
        if ($order->getShippingDate()->getTimestamp() < $curdate->getTimestamp()) {
            return new Response("kargoya verilen sipariş güncellenemez!", "400");
        }

        /** @var Order $newOrder */
        $newOrder = $this->deserialize($request->getContent(), Order::class);
        if ($order->getId() !== (int)$id) {
            return new Response("id girilemez", "400");
        }

        $em = $this->getDoctrine()->getManager();
        $em->persist($newOrder);
        $em->flush();
        $em->refresh($newOrder);
        return $this->response($newOrder, ['Default','items', 'user']);
    }

    /**
     *
     * @Route("/order/{id}", methods={"GET"})
     *
     * @OA\Response(
     *     response=200,
     *     description="Returns the order",
     *     @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref=@Model(type=Order::class, groups={"Default","items","user"}))
     *     )
     * )
     * @OA\Tag(name="order")
     * @Security(name="Bearer")
     *
     */
    public function order($id)
    {
        $em = $this->getDoctrine()->getManager();
        $order = $em->getRepository(Order::class)->find($id);
        if (!$order) {
            return new Response("sipariş bulunamadı", "404");
        }
        if ($order->getUser()->getId() != $this->getUser()->getId()) {
            return new Response("size ait olmayan siparişin detaylarını göremezsiniz", "400");
        }
        return $this->response($order, ['Default','items','user']);
    }
}