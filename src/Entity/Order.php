<?php

namespace App\Entity;

use App\Repository\OrderRepository;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity(repositoryClass=OrderRepository::class)
 * @ORM\Table(name="`order`")
 */
class Order
{

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $code;

    /**
     * @ORM\Column(type="datetime")
     */
    private $shippingDate;

    /**
     * @ORM\Column(type="string")
     */
    private $address;

    /**
     * @Serializer\Groups({"items"})
     * @var ArrayCollection $items
     * @ORM\OneToMany(targetEntity="App\Entity\OrderItem", mappedBy="order", cascade={"remove", "persist", "refresh", "merge", "detach"})
     * @ORM\JoinColumn(referencedColumnName="order_id", nullable=false)
     * @ORM\OrderBy({"id" = "ASC"})
     */
    private $items;

    /**
     * @var User
     * @Serializer\Groups({"user"})
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getShippingDate(): ?DateTime
    {
        return $this->shippingDate;
    }

    /**
     * @param mixed $shippingDate
     */
    public function setShippingDate($shippingDate): void
    {
        $this->shippingDate = $shippingDate;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $address
     */
    public function setAddress($address): void
    {
        $this->address = $address;
    }

    /**
     * @return ArrayCollection | PersistentCollection
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @param ArrayCollection $items
     */
    public function setItems(ArrayCollection $items): void
    {
        $this->items = $items;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser(User $user): void
    {
        $this->user = $user;
    }

    /**
     * Add RecipeItem
     * @param OrderItem $orderItem
     * @return Order
     */
    public function addItem(OrderItem $orderItem)
    {
        $this->items[] = $orderItem;
        $orderItem->setOrder($this);
        return $this;
    }

    /**
     * Remove OrderItem
     * @param OrderItem $orderItem
     */
    public function removeItem(OrderItem $orderItem)
    {
        $this->items->removeElement($orderItem);
    }
}
